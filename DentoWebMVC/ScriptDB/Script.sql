USE [master]
GO
/****** Object:  Database [DentoWebMaster]    Script Date: 18/05/2021 17:42:41 ******/
CREATE DATABASE [DentoWebMaster]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DentoWebMaster', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\DentoWebMaster.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DentoWebMaster_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\DentoWebMaster_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [DentoWebMaster] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DentoWebMaster].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DentoWebMaster] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DentoWebMaster] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DentoWebMaster] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DentoWebMaster] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DentoWebMaster] SET ARITHABORT OFF 
GO
ALTER DATABASE [DentoWebMaster] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DentoWebMaster] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DentoWebMaster] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DentoWebMaster] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DentoWebMaster] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DentoWebMaster] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DentoWebMaster] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DentoWebMaster] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DentoWebMaster] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DentoWebMaster] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DentoWebMaster] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DentoWebMaster] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DentoWebMaster] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DentoWebMaster] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DentoWebMaster] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DentoWebMaster] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DentoWebMaster] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DentoWebMaster] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DentoWebMaster] SET  MULTI_USER 
GO
ALTER DATABASE [DentoWebMaster] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DentoWebMaster] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DentoWebMaster] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DentoWebMaster] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DentoWebMaster] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DentoWebMaster] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [DentoWebMaster] SET QUERY_STORE = OFF
GO
USE [DentoWebMaster]
GO
/****** Object:  Table [dbo].[Cita]    Script Date: 18/05/2021 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cita](
	[idCita] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [date] NULL,
	[idHorario] [int] NULL,
	[estado] [varchar](10) NULL,
	[pago] [varchar](50) NULL,
	[idCliente] [int] NULL,
	[idDoctor] [int] NULL,
	[monto] [decimal](8, 4) NULL,
 CONSTRAINT [PK__Cita__814F312620148252] PRIMARY KEY CLUSTERED 
(
	[idCita] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 18/05/2021 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NULL,
	[nombres] [varchar](50) NULL,
	[apellidos] [varchar](50) NULL,
	[dni] [nvarchar](8) NULL,
	[fechaNac] [date] NULL,
	[correo] [varchar](100) NULL,
	[telefono] [varchar](20) NULL,
	[usuario] [varchar](50) NULL,
	[passwd] [varchar](200) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 18/05/2021 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[idDoctor] [int] IDENTITY(1,1) NOT NULL,
	[nombres] [varchar](50) NULL,
	[apellidos] [varchar](50) NULL,
	[codigoCol] [varchar](50) NULL,
	[casaEstudio] [varchar](100) NULL,
	[titulo] [varchar](100) NULL,
	[dni] [nvarchar](8) NULL,
	[correo] [varchar](100) NULL,
	[telefono] [varchar](20) NULL,
	[usuario] [varchar](50) NULL,
	[passwd] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[idDoctor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Historia]    Script Date: 18/05/2021 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Historia](
	[idHistoria] [int] IDENTITY(1,1) NOT NULL,
	[idCita] [int] NULL,
	[observacion] [varchar](500) NULL,
	[motivo] [varchar](500) NULL,
	[fecha] [date] NULL,
	[descripcion] [varchar](500) NULL,
	[examenes] [varchar](500) NULL,
	[diagnostico] [varchar](500) NULL,
	[tratamiento] [varchar](500) NULL,
 CONSTRAINT [PK_Historia] PRIMARY KEY CLUSTERED 
(
	[idHistoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario]    Script Date: 18/05/2021 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario](
	[idHorario] [int] IDENTITY(1,1) NOT NULL,
	[horaInicio] [varchar](10) NULL,
	[horaFin] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[idHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [DentoWebMaster] SET  READ_WRITE 
GO
